<?php

use Illuminate\Database\Seeder;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();
        $categories = array(
            array('cat_name' => 'Cat.A'),
            array('cat_name' => 'Cat.B'),
        );

        DB::table('categories')->insert($categories);
    }
}
