// *
// * Add multiple markers
// * 2013 - en.marno



var map;
var Markers = {};
var infowindow;

var base_url = location;
var new_url = base_url + "search/autocomplete";


//show marker
$(document).on('click','.goto',function(e) {
    e.preventDefault();

    var long = $(this).attr("data-long");
    var lat = $(this).attr("data-lat");
    var name = $(this).data("title");
    var address = $(this).data("address");
    var id = $(this).data("id");
    var zipcode = $(this).data("zip");

    var latlng = new google.maps.LatLng(lat,long);

    addmarker(latlng, name, address, zipcode, id);

});




function initialize() {
    var maxLong = $('#maxLong').val();
    var maxLat = $('#maxLat').val();

    var origin = new google.maps.LatLng(maxLat, maxLong);

    var mapOptions = {
        zoom: 4,
        center: origin
    };

    map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

    infowindow = new google.maps.InfoWindow();

    $.getJSON(new_url, function(data){

        for(i=0; i < data.length; i++) {
            var position = new google.maps.LatLng(data[i].latitude, data[i].longitude);

            var event_id = data[i].id;
            var name = data[i].headline;
            var address = data[i].address;
            var zipcode = data[i].zipcode;

            createMarker(position, name, address, zipcode,event_id);
        }
    });
    //locate(0);

}

function createMarker(latlng, name, address, zipcode, id){
    var marker = new google.maps.Marker({
        map: map,
        position: latlng,
        title: name
    });

    google.maps.event.addListener(marker, 'click', function() {

        // Creating the content to be inserted in the infowindow
        var iwContent = '<div id="iw_container">' +
            '<div class="iw_title">' + name + '</div>' +
            '<div class="iw_content">' + address + '<br />' +
            zipcode + '<br /><a href="" class="rsvp_me" id="'+ id +'" data-id="'+ id +'" data-toggle="modal" data-target="#myModal">RSVP Event</a></div></div>';

        // including content to the Info Window.
        infowindow.setContent(iwContent);
        infowindow.setOptions({maxWidth: 200});
        infowindow.open(map, marker);
    });

}

google.maps.event.addDomListener(window, 'load', initialize);



function addmarker(latlong, name, address, zipcode, id) {
    var marker = new google.maps.Marker({
        position: latlong,
        title: 'new marker',
        map: map,
        zoom: 13
    });

    map.setCenter(marker.getPosition());
    map.setZoom(10);

    createMarker(latlong, name, address, zipcode, id);

}


$(document).on('click','.cat',function(e) {
    e.preventDefault();

    var cat = $(this).data("id");
    new_url = base_url + "cat/" + cat ;

    var marker = new google.maps.Marker({
        map: map,
        zoom: 13
    });

    map.setCenter(marker.getPosition());
    map.setZoom(10);

    $.getJSON(new_url, function(data){
        for(i=0; i < data.length; i++) {
            var holder = data[i].event; //find event in category
            for(j = 0; j < holder.length; j++){

                var position = new google.maps.LatLng(holder[j].latitude, holder[j].longitude);
                var name = holder[j].headline;
                var address = holder[j].address;
                var zipcode = holder[j].zipcode;
                var event_id = holder[j].id;

                createMarker(position, name, address, zipcode,event_id);
            }
        }
    });

});
