/**
 * Created by Didi on 30/08/2016.
 */

jQuery(function($) {

    var base_url = location;
    //alert(base_url);
    var new_url = base_url + "sort";

	var datePicker = { dateFormat: 'yy-mm-dd',
                changeMonth: true,
                changeYear: true };

                $('.newstartdate').datepicker(datePicker); //trigger date picker
        	$('.newenddate').datepicker(datePicker);

    $('#myModal').on('show.bs.modal', function (e) {
        var eventNumber = $(e.relatedTarget).attr('data-id');
        $(this).find('.eventnumber').val(eventNumber);
    });



    //$(document).on('click','#rsvp_button',function(e) {
    //        e.preventDefault();
    //
    //    var token = $('[name="_token"]').val();
    //
    //    $.ajax({
    //        url:base_url+"rsvp",
    //        data: token,
    //        type:'POST',
    //        success: function(data){
    //            console.log(data);
    //        }
    //
    //    });
    //});



    var panelList = $('#accordion');

    panelList.sortable({
        axis: 'y',
        handle: '.panel-heading',
        //stop: function() {
        //    alert(panelList.sortable('serialize'));
        //},
        update : function (event, ui) {
            var data = $(this).sortable('serialize');
            console.log(data);

            $.ajax({
                data: data,
                type: 'POST',
                url: new_url

            });

        }
    });


    $( "#q" ).autocomplete({
        source: new_url,
        minLength: 5,
        select: function(event, ui) {
            $('#q').val(ui.item.country);
        }

    });

});
