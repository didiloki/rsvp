@extends('layouts.app')

@section('content')
    <style>
        .navbar{
            margin-bottom: 0px;
        }
    </style>

    <!-- Get the long and lat-->
    <input type="hidden" id="maxLong" value="{{ $maxLong }}" />
    <input type="hidden" id="maxLat" value="{{ $maxLat }}" />

<div id="map_wrapper">
    <div id="map-canvas" class="mapping" style="height: 400px;"></div>
    <div class="mobile-view">
<img src="https://maps.googleapis.com/maps/api/staticmap?center={{ $maxLat }},{{ $maxLong }}zoom=2&size=400x200&maptype=roadmap
        @foreach($events as $event)
            @if(count($event) > 0)
                &markers={{ $event->latitude }},{{ $event->longitude }}
            @endif
        @endforeach
                " />
    </div>
</div>


    <div class="well">
        <div class="container">
            <div class="col-md-7 col-md-offset-1">
                {{Form::open(['action' => 'EventController@search', 'method'=> 'GET'])}}
                {{ Form::text('q', '', ['id' =>  'q', 'class'=> 'form-control', 'placeholder' =>  'Zipcode, Address,Country'])}}
                {{Form::close()}}
            </div>
            <div class="col-md-3">
                <div>
                    <button class="btn btn-default cat" data-id="1">Cat A</button>
                    <button class="btn btn-default cat" data-id="2">Cat B</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">

            <div class="col-md-12 col-md-offset-1">
                <div class="col-md-7">

                    <div class="well" style="padding: 0px;">
                        <div class="panel-heading">CAT.A</div>
                    </div>

                    @foreach($eventa as $event)
                        @if(count($event) > 0)
                        <div class="item borderevent col-md-6" id="event-A"  style="position: relative;">
                            <div class="col-md-9 no-margin no-padding">
                                <span class="dates">{{ $event->start_date }} - {{ $event->end_date }}</span>
                                <span class="headline">{{ $event->headline }}</span>

                                @foreach($countries as $country)

                                    @if($event->country_id == $country->id)
                                        <span> {{ $country->code }}/ {{ $country->name }}</span>
                                    @endif
                                @endforeach
                            </div>
                            <div class="col-md-3 no-margin no-padding">
                                    @foreach($category as $cat)

                                        @if($event->cat_id == $cat->id)
                                            <span> {{ $cat->cat_name }}</span>
                                        @endif
                                    @endforeach

                                <span>

                                    {{ $event->guests->count() }}
                                </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="move">

                                <a href="#" class="btn btn-default goto" data-id="{{ $event->id }}" data-title="{{ $event->headline }}" data-address="{{ $event->address }}" data-zip="{{ $event->zip }}" data-long="{{ $event->longitude }}" data-lat="{{ $event->latitude }}">Show on map</a>
                            </div>
                        </div>
                        @else
                            <div class="alert alert-danger"> No Event Yet! </div>
                            @endif
                    @endforeach

                </div>
                <div class="col-md-3">
                    <div class="well" style="padding: 0px;">
                        <div class="panel-heading">CAT.B</div>
                    </div>
                    <div class="panel-body" style="padding: 0px;">
                        @foreach($eventb as $eventb)
                            @if(count($eventb) > 0)
                            <div class="item borderevent col-md-12" id="event-A" style="position: relative;" >
                                <div class="col-md-9 no-margin no-padding">
                                    <span class="dates">{{ $eventb->start_date }} - {{ $eventb->end_date }}</span>
                                    <span class="headline">{{ $eventb->headline }}</span>

                                    @foreach($countries as $country)

                                        @if($eventb->country_id == $country->id)
                                            <span> {{ $country->code }}/ {{ $country->name }}</span>
                                        @endif
                                    @endforeach
                                </div>
                                <div class="col-md-3 no-margin no-padding">
                                    @foreach($category as $cat)

                                        @if($eventb->cat_id == $cat->id)
                                            <span> {{ $cat->cat_name }}</span>
                                        @endif
                                    @endforeach
                                    <span>
                                       {{ $eventb->guests->count() }}
                                    </span>
                                </div>
                                <div class="clearfix"></div>
                                <div class="move">

                                    <a href="#" class="btn btn-default goto" data-id="{{ $eventb->id }}" data-title="{{ $eventb->headline }}" data-address="{{ $eventb->address }}" data-zip="{{ $eventb->zip }}" data-long="{{ $eventb->longitude }}" data-lat="{{ $eventb->latitude }}">Show on map</a>
                                </div>
                            </div>
                            @else
                                <div class="alert" > No Event Yet! </div>
                            @endif
                            @endforeach
                    </div>

                </div>
            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">RSVP this Event!</h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(['url' => 'rsvp', 'method' => 'POST', 'class'=> 'rsvp_form']) }}
                    <div>
                        {{ Form::text('name', '',["class" => "form-control", "placeholder"=>"John Doe"]) }}
                    </div>
                    <div>
                        {{ Form::text('email', '',["class" => "form-control", "placeholder"=>"john.doe@example.com"]) }}
                    </div>
                    {{ Form::hidden('event_id', '', ["class"=> "eventnumber"]) }}
                    {{ Form::submit('submit', ["class"=> "btn btn-block", "id"=>"rsvp_button"]) }}
                    {{ Form::close() }}
                </div>

            </div>
        </div>
    </div>


@endsection