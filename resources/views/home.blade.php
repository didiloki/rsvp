@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="panel-group" id="accordion">

            @foreach($events as $event)
                <div class="panel panel-default" id="item_{{$event->id}}">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a href="#collapse{{$event->id}}" data-toggle="collapse" data-parent="#accordion">
                                <b>{{ $event->headline }}</b> / {{ $event->start_date }} - {{$event->end_date}}</a>

                            <div class="pull-right">
                                <a href="{{ route('events.edit', $event->id) }}" class="btn btn-default btn-sm edit"> Edit</a>
                                <a href="#collapse{{$event->id}}"  data-toggle="collapse" data-parent="#accordion" class="btn btn-default btn-sm"> Hide</a>
                                <a href="{{ route('events.destroy', $event->id) }}" class="btn btn-danger btn-sm trash"> Delete</a>

                            </div>
                        </h4>
                    </div>
                    <div id="collapse{{$event->id}}" class="panel-collapse collapse">
                        {{ Form::model($event,['route' => ['events.update', $event->id], 'method' => 'PATCH']) }}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="headline">
                                            Headline</label>
                                        {{ Form::text('headline', null, ['class' => 'form-control', 'placeholder'=>'Headline']) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="description">
                                            Description</label>
                                        {{ Form::textarea('description', null, ['class'=>'form-control','placeholder'=>'Description','rows'=>'5' ]) }}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address">
                                            Address</label>
                                        {{ Form::text('address', null, array('class' => 'form-control', 'placeholder'=>'Address')) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="zipcode">
                                            Zip Code</label>
                                        {{ Form::text('zip', null, array('class' => 'form-control', 'placeholder'=>'MK18 1PY')) }}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="country">
                                            Country</label>
                                        <select class="form-control" id="country" name="country">
                                            @foreach($countries as $country)
                                                <option value="{{ $country->id }}"
                                                        @if($event->country_id == $country->id)
                                                        selected
                                                        @endif
                                                >{{ $country->name }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="category">
                                            Category</label>
                                        <select class="form-control" id="category" name="category">
                                            @foreach($categories as $category)
                                                <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>


                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="startdate">
                                            Start Date</label>
                                        {{ Form::text('start_date', null, ['id' => 'startdate', 'placeholder'=> '', 'class'=> 'form-control startdate']) }}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tags">
                                            End Date</label>
                                        {{ Form::text('end_date', null, ['id' => 'enddate', 'placeholder'=> '', 'class'=> 'form-control enddate']) }}
                                    </div>
                                </div>
                            </div>

                            {{ Form::submit('Save', '',['class'=>'form-control']) }}
                        </div>
                        {{ Form::close() }}
                    </div>
                </div>
            @endforeach

        </div>
    </div>

    <script>
        $(document).ready(function(){
        $(document).on('click','.trash',function(e){
            e.preventDefault();
            var url = $(this).attr("href");

            var confirm = window.confirm("Are you sure you want to delete?");
            if (confirm) {

                $.ajax({
                    url:url,
                    data: { "_token":"{{ csrf_token() }}" },
                    type:'DELETE',
                    success: function(data){
                        if(data == 0){
                            $(this).fadeOut( 1600, "linear", complete );
                            alert("deleted the row");
                            //$ele.fadeOut().remove();
                        }else{
                            alert("can't delete the row");
                        }

                        console.log(data);
                    },
                    complete:function(data){

                    }

                });
            }
        });
        });
    </script>

    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            <a href="{{ route('events.create') }}" class="btn btn-default btn-block add_button">Add New Event</a>
        </div>
    </div>

@endsection
