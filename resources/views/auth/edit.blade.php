{{ Form::model($event,['route' => ['events.update', $event->id], 'method' => 'PATCH']) }}
<div class="panel-body">
    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label for="headline">
                    Headline</label>
                {{ Form::text('headline', null, ['class' => 'form-control', 'placeholder'=>'Headline']) }}
            </div>
            <div class="form-group">
                <label for="description">
                    Description</label>
                {{ Form::textarea('description', null, ['class'=>'form-control','placeholder'=>'Description','rows'=>'5' ]) }}
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="address">
                    Address</label>
                {{ Form::text('address', null, array('class' => 'form-control', 'placeholder'=>'Address')) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="zipcode">
                    Zip Code</label>
                {{ Form::text('zip', null, array('class' => 'form-control', 'placeholder'=>'MK18 1PY')) }}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="country">
                    Country</label>
                <select class="form-control" id="country" name="country">
                    @foreach($countries as $country)
                        <option value="{{ $country->id }}"
                                @if($event->country_id == $country->id)
                                selected
                                @endif
                        >{{ $country->name }}</option>

                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="category">
                    Category</label>
                <select class="form-control" id="category" name="category">
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                    @endforeach
                </select>
            </div>
        </div>


    </div>


    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="startdate">
                    Start Date</label>
                {{ Form::text('start_date', null, ['id' => 'startdate', 'placeholder'=> '', 'class'=> 'form-control startdate']) }}
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="tags">
                    End Date</label>
                {{ Form::text('end_date', null, ['id' => 'enddate', 'placeholder'=> '', 'class'=> 'form-control enddate']) }}
            </div>
        </div>
    </div>

    {{ Form::submit('Save', '',['class'=>'form-control']) }}
</div>
{{ Form::close() }}