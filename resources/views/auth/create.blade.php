@extends('layouts.app')

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places"> </script>
    <script type="text/javascript" src="{{ asset('js/old-maps.js') }}" ></script>
@endsection

@section('content')

<div class="container">
<div class="template">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a href="#collapse">New Event</a>
            </h4>
        </div>
        <div id="collapse" class="panel-collapse collapse in">
            {{ Form::open(['route' => 'events.store', 'method' => 'POST']) }}
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="headline">
                                Headline</label>
                            {{ Form::text('headline', '', ['class' => 'form-control', 'placeholder'=>'Headline']) }}
                            @if ($errors->has('headline'))<p style="color:red;">{!!$errors->first('headline')!!}</p>@endif

                        </div>
                        <div class="form-group">
                            <label for="description">
                                Description</label>
                            {{ Form::textarea('description', '', ['class'=>'form-control','placeholder'=>'Description','rows'=>'5' ]) }}
                            @if ($errors->has('description'))<p style="color:red;">{!!$errors->first('description')!!}</p>@endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="address">
                                Address</label>
                            {{ Form::text('address', '', array('class' => 'form-control', 'placeholder'=>'Address')) }}
                            @if ($errors->has('address'))<p style="color:red;">{!!$errors->first('address')!!}</p>@endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="zipcode">
                                Zip Code</label>
                            {{ Form::text('zip', '', array('class' => 'form-control', 'placeholder'=>'MK18 1PY')) }}
                            @if ($errors->has('zip'))<p style="color:red;">{!!$errors->first('zip')!!}</p>@endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="country">
                                Country</label>
                            <select class="form-control" id="country" name="country">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('country_id'))<p style="color:red;">{!!$errors->first('country_id')!!}</p>@endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="category">
                                Category</label>
                            <select class="form-control" id="category" name="category">
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->cat_name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                </div>


                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="startdate">
                                Start Date</label>
                            {{ Form::text('start_date', '', ['id' => 'startdate', 'placeholder'=> '', 'class'=> 'form-control newstartdate']) }}
                            @if ($errors->has('start_date'))<p style="color:red;">{!!$errors->first('start_date')!!}</p>@endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="tags">
                                End Date</label>
                            {{ Form::text('end_date', '', ['id' => 'enddate', 'placeholder'=> '', 'class'=> 'form-control newenddate']) }}
                            @if ($errors->has('end_date'))<p style="color:red;">{!!$errors->first('end_date')!!}</p>@endif
                        </div>
                    </div>
                </div>

                {{ Form::submit('Save', '',['class'=>'form-control']) }}
            </div>
            {{ Form::close() }}

        </div>
    </div>

</div>


    <div id="locationField">
        <input id="autocomplete" placeholder="Enter your address"
               onFocus="geolocate()" type="text"></input>
    </div>

    <table id="address">
        <tr>
            <td class="label">Street address</td>
            <td class="slimField"><input class="field" id="street_number"
                                         disabled="true"></input></td>
            <td class="wideField" colspan="2"><input class="field" id="route"
                                                     disabled="true"></input></td>
        </tr>
        <tr>
            <td class="label">City</td>
            <td class="wideField" colspan="3"><input class="field" id="locality"
                                                     disabled="true"></input></td>
        </tr>
        <tr>
            <td class="label">State</td>
            <td class="slimField"><input class="field"
                                         id="administrative_area_level_1" disabled="true"></input></td>
            <td class="label">Zip code</td>
            <td class="wideField"><input class="field" id="postal_code"
                                         disabled="true"></input></td>
        </tr>
        <tr>
            <td class="label">Country</td>
            <td class="wideField" colspan="3"><input class="field"
                                                     id="country" disabled="true"></input></td>
        </tr>
    </table>


</div>


@endsection