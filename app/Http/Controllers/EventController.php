<?php

namespace App\Http\Controllers;

use App\Models\Guest;
use Illuminate\Http\Request;

use App\Http\Requests\CreateEventRequest;


use App\Http\Requests;

use App\Models\Category;
use App\Models\Countries;
use App\Models\Event;
use Illuminate\Support\Facades\Auth;
use Response;

use Socialite;
use Session;

use View;

use Illuminate\Support\Facades\DB;

class EventController extends Controller
{

    protected $events;
    protected $countries;
    protected $category;

    public function __construct()
    {
        $this->events = Event::all();
        $this->countries = Countries::all();
        $this->category = Category::all();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $eventa = Event::with('guests')->where('cat_id', '=', 1)
            ->orderBy('created_at', 'desc')->get();
        //return $eventa;


        $eventb = Event::with('guests')->where('cat_id', '=', 2)
                ->orderBy('created_at', 'desc')->get();

        $biggestLong = $this->maxLong(); // get max longitude
        $biggestLat = $this->maxLat(); //get max latitude
	
        return view::make('welcome', ['events' => $this->events,
            'eventa' => $eventa,
            'eventb' => $eventb,
            'category' => $this->category,
            'countries' => $this->countries,
            'maxLong' => $biggestLong,
            'maxLat'=> $biggestLat ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view::make('auth.create', ['events' => $this->events,
            'categories' => $this->category,
            'countries' => $this->countries]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\CreateEventRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEventRequest $request)
    {
        $event = new Event();

	$countryName = DB::table('countries')
            ->where('id', '=',$request->input('country'))
            ->first();

	/* get the long and latitude from google maps*/
        $address = $request->input('address').','. $request->input('zip'). ',' . $countryName->name ;
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');

        $output= json_decode($geocode);
        
        if(isset($output)){

        /* data to input */
        $event->latitude = $output->results[0]->geometry->location->lat;
        $event->longitude = $output->results[0]->geometry->location->lng;

        $event->headline = $request->input('headline');
        $event->description = $request->input('description');
        $event->address = $request->input('address');

        $event->zip = $request->input('zip');
        $event->start_date = $request->input('start_date');
        $event->end_date = $request->input('end_date');

        //$event->visible = 0;

        $event->position = 0;

        $event->cat_id = $request->input('category');
        $event->user_id = Auth::user()->id;
        $event->country_id = $request->input('country');


        $event->save();

        return  view::make('home',['categories' => $this->category,
            'events' => $this->events,
            'countries' => $this->countries,
            'success'=> 'Successfully Added New Event!']);
            }
            else{
                return  view::make('home',['categories' => $this->category,
                    'events' => $this->events,
                    'countries' => $this->countries,
                    'Failed'=> 'Failed to add New Event!']);
            }
           // }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       return 0;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);

        //$event->update(Input::except('_token'));

        $data = $request->all();

        $event->fill($data)->save();

        Session::flash('message', 'Successfully!');

        return  view::make('home',['categories' => $this->category,
            'events' => $this->events,
            'countries' => $this->countries]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);

        $event->delete();

        return 0;
    }

    public function search()
    {
        return  $this->events;

    }

    public function displayCatOnMap($id)
    {
        $queries = Category::with('event')
            ->where('id', '=', $id)
            ->get();
        return $queries;
    }

    /* find max and min long and lat
    */

    public function maxLong()
    {

        $queriesLong = DB::table('events')
            ->select(DB::raw('max(longitude) as longitude'))
            ->first();

        return $queriesLong->longitude;
    }
    
    public function maxLat()
    {

        $queriesLong = DB::table('events')
            ->select(DB::raw('max(latitude) as latitude'))
            ->first();

        return $queriesLong->latitude;
    }



    //RSVP events
    public function rsvpEvent(Request $request)
    {
        $guest = new Guest();

        $guest->name = $request->input('name');
        $guest->email = $request->input('email');
        $event_id = $request->input('event_id'); //event id

        //check if it exists
        $guestCheck = Guest::where('email',$guest->email)->first();
        $evnt = Event::find($event_id);


        //check if guest exists returns null
        if (is_null($guestCheck) || empty($guestCheck)) {

            $guest->save();
            //link guest to event
            $evnt->guests()->attach($guestCheck->id);

            return "its done!";

        }elseif(!empty($guestCheck)){

            $evnt->guests()->attach($guestCheck->id);

            return "its done!";

        }else{
            return "already signed up";
        }





       // return ;
    }

    //RSVP events
    public function rsvpCheck()
    {
//        $check = Event::find(5);
//        $check->guests()->attach(2);
//
//        return $check->guests;

        $check = Guest::find(1);
        $check->events()->attach(6);

        return $check->events;


    }

    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function callback()
    {

        return redirect()->to('/events');
    }
}