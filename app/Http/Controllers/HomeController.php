<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Countries;
use App\Models\Event;

use View;

class HomeController extends Controller
{
    protected $events;
    protected $countries;
    protected $category;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->events = Event::all();
        $this->countries = Countries::all();
        $this->category = Category::all();

        View::share('category', $this->category);
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        return view::make('home', ['categories' => $this->category,
                                    'events' => $this->events,
                                    'countries' => $this->countries]);
    }
}
