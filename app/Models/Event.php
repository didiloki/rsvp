<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['headline', 'description', 'address', 'zip', 'longitude', 'latitude',
        'position', 'country_id', 'cat_id', 'start_date', 'end_date'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * The products that belong to the shop.
     */
    public function guests()
    {
        return $this->belongsToMany('App\Models\Guest', 'event_guest');
    }

    /**
     * The products that belong to the shop.
     */
    public function category()
    {
        return $this->hasOne('App\Models\Category');
    }
}
