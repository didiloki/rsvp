<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    //
    protected $fillable = ['first_name', 'last_name', 'email'];

    public function events()
    {
        return $this->belongsToMany('App\Models\Event', 'event_guest');//, 'event_id', 'guest_id');
    }
}
