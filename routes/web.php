<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/','EventController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::resource('/events','EventController');

Route::get('/redirect', 'EventController@redirect');
Route::get('/callback', 'EventController@callback');


Route::post('/rsvp', 'EventController@rsvpEvent');

Route::get('/cat/{id}', 'EventController@displayCatOnMap');

Route::get('/search/autocomplete', 'EventController@search');